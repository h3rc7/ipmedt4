//importeren benodigde componenten
import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import NsApi from './NsApi'
import Storingen from "./Storingen";
import Werkzaamheden from "./Werkzaamheden";
import Klachten from "./Klachten";
import Container from '@material-ui/core/Container';

// Functie voor tabs
function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};
// Bottembar style
const useStyles = makeStyles(theme => ({
  root: {

  },
  stickToBottom: {
  maxWidth: '560px',
  minWidth: '10px',
  position: 'fixed',
  right: "auto",
  bottom: 0,
  top: "auto",
},
}));

// Afhandelen van de tabs
function FullWidthTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  function handleChangeIndex(index) {
    setValue(index);
  }
//Bottembar weergeven
  return (
    <div id="bottomMenu" className={classes.root}>

      <SwipeableViews axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={value} onChangeIndex={handleChangeIndex}>
      <Container maxWidth="sm" fixed>
        <TabContainer dir={theme.direction}>
        <NsApi/>
        </TabContainer>
        </Container>
        <Container maxWidth="sm">
        <TabContainer dir={theme.direction} onClick={Storingen.gettripsStoringen} >
        <Storingen/>
        </TabContainer>
        </Container>
        <Container maxWidth="sm">
        <TabContainer dir={theme.direction}>
        <Werkzaamheden/>
        </TabContainer>
        </Container>
        <Container maxWidth="sm">
        <TabContainer dir={theme.direction}>
        <Klachten/>
        </TabContainer>
        </Container>
      </SwipeableViews>
          <Container maxWidth="sm">
      <AppBar color="default" className={classes.stickToBottom}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          centered="true"
        >
          <Tab label="Ritten" />
          <Tab label="Storingen" />
          <Tab label="Werkzaamheid" />
          <Tab label="Klachten" />
        </Tabs>
      </AppBar>
      </Container>
    </div>
  );
}

export default FullWidthTabs;
