//importeren benodigde componenten
import React from "react";
import './App.css';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import axios from 'axios';

//Class voor klachten
class Klachten extends React.Component{
  constructor(props){
    super(props);
    this.getKlachten();
  }

  state = {
    isLoading: true,
    errors: null
  };
//Ophalen van de klachten van de api
  getKlachten = () =>{
    var url = 'http://127.0.0.1:8000/api/klachten';
    axios.get(url)
      .then(response => {
        console.log(response)
        this.setState({
          trips: response.data.result,
          isLoading: false
        });
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }
// Renderen en weergeven van de klachten die opgehaald worden door de api
  render(){
      const { isLoading, trips } = this.state;
    return(
      <div>
        <div className="tijd_div_topbar tijd_div_topbar--red">
            <AnnouncementIcon color="disabled" style={{ fontSize: 30, marginTop: -9, marginRight: -25, marginLeft: 26  }}/>
            <h4 className="tijd_div_topbar__h4--orange">Klachten </h4>
        </div>
        <div className="tijd_div tijd_div--modifier">
          { !isLoading && trips.length > 0 ? (
            trips.map(trip => {
              const { naam, klacht, id, vanNaar} = trip;
              return(
                <div key={id} id="storing_enkelblok" >
                  <h2>Inkomende Klacht</h2>
                  <p><b>Naam:</b> {naam ||"info niet beschikbaar"}</p>
                  <p><b>van/naar:</b> {vanNaar ||"info niet beschikbaar"}</p>
                  <p><b>Klacht:</b> {klacht ||"info niet beschikbaar"}</p>
                  <hr className="hr__storing_enkelblok" />
                </div>
              );
            })
          ) : (
            <div id="storing" >
              <h3>Er zijn op dit moment geen klachten</h3>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default Klachten;
