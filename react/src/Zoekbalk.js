//importeren benodigde componenten
import React from "react";

import TextField from '@material-ui/core/TextField';
import './App.css';
import Grid from '@material-ui/core/Grid';

import Fab from '@material-ui/core/Fab';
import IntegrationAutosuggest from "./IntegrationAutosuggest";


// Class voor zoekbalk
class Searchbar extends React.Component{
  // constructor voor het setten van vertrek datum, tijd en aankomst
  constructor(props){
    super(props);
    var tijdIso = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('.')[0];
    tijdIso = tijdIso.slice(0, -3)
    this.state = {vertrek: "", aankomst: "", tijd: tijdIso};
    this.handleVertrekChange = this.handleVertrekChange.bind(this);
    this.handleAankomstChange = this.handleAankomstChange.bind(this);
    this.handleTijd = this.handleTijd.bind(this);
  }
// handelen van vertrek
  handleVertrekChange (evt, newValue) {
    if (newValue !== '')
      this.setState({ vertrek: newValue });
  }
// handelen van aankomst
  handleAankomstChange (evt, newValue) {
    if (newValue !== '')
    this.setState({ aankomst: newValue });
  }
// handelen van tijd
  handleTijd (evt) {
    this.setState({ tijd: evt.target.value });
  }
// submitten van de waardes en opslaan in states
  onSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.vertrek, this.state.aankomst, this.state.tijd);
  };

// renderen en weergeven van de zoekbalk
  render(){

    return(
    <div>
    <div className="tijd_div_topbar">
        <h4 className="tijd_div_topbar__h4">Waar wil je heen?</h4>
    </div>
      <div className="tijd_div" >
        <form onSubmit={this.onSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
            <IntegrationAutosuggest
              onChangeValue={this.handleVertrekChange}
              label="Van"
            />
              </Grid>

              <Grid item xs={8}>
                <TextField
                style={{ zIndex: '0' }}
                 id="datetime-local"
                 label="Datum -&- Tijd"
                 variant="outlined"
                 type="datetime-local"
                 onChange={this.handleTijd}
                 value={this.state.tijd}
                 InputLabelProps={{shrink: true,}}
                 />
              </Grid>

              <Grid item xs={12}>
              <IntegrationAutosuggest
                onChangeValue={this.handleAankomstChange}
                label="Naar"
              />
              </Grid>
            </Grid>
            <Fab
              id="submit_knop"
              variant="extended"
              size="medium"
              type="submit"
              color="primary"
              aria-label="Add"> Zoek
            </Fab>
         </form>
      </div>
    </div>
    );
  }
}
export default Searchbar;
