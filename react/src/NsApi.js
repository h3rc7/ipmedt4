// Importeren van benodigde componenten
import React from 'react';
import axios from 'axios';
import Zoekbalk from './Zoekbalk';
import CircularUnderLoad from "./CircularUnderLoad";
import LinearProgress from '@material-ui/core/LinearProgress';
import AddIcon from '@material-ui/icons/Add';
import ArrowForward from '@material-ui/icons/ArrowForward';

import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';

import GroupIcon from '@material-ui/icons/Group';
import DepartureBoard from '@material-ui/icons/DepartureBoard';
import AvTimer from '@material-ui/icons/AvTimer';
import Timelapse from '@material-ui/icons/Timelapse';
import TextField from '@material-ui/core/TextField';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Fab from '@material-ui/core/Fab';



// Functie voor het formatteren van de data/tijdstip zodat het leesbaar is    ISOString -> leesbaar
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes;
  return strTime;
}

const frtButton = { backgroundColor: "rgb(225, 0, 80)",};
const scdButton = { backgroundColor: "rgb(225, 125, 0)",};
const fohButton = { backgroundColor: "#ffc107",};


  class TreinInfo extends React.Component {





    state = {
      // alle ritten
      trips: [],
      // alle meldingen
      meldingen: [],
      // states voor material ui
      isLoading: true,
      errors: null,
      value: false,
      anchorEl: null,
      showModal: false,
      naam: '',
      vanNaar: '',
      email: '',
      klacht: ''
    };

    //functies voor de states
    handlenaamChange(e) {
      this.setState ({
        naam: e.target.value
      })
    }


    handlevanNaarChange(e) {
      this.setState ({
        vanNaar: e.target.value
      })
    }

    handleemailChange(e) {
      this.setState ({
        email: e.target.value
      })
    }

    handleklachtChange(e) {
      this.setState ({
        klacht: e.target.value
      })
    }

    handleSubmit(e) {
      e.preventDefault();
      console.log(this.state);
        axios({
            method: 'post',
            url: 'http://127.0.0.1:8000/api/klacht',
            headers: {},
            data: {
              "naam": this.state.naam,
              "vanNaar": this.state.vanNaar,
              "email": this.state.email,
              "klacht": this.state.klacht,
            }
            })  .then(function (response) {
            })
          }


     toggleModal = () => {
       this.setState({
         showModal: !this.state.showModal
       });
     }

// onsubmit wordt de meldingen van de laravel API opgehaald en de ritten van NS API
    onSubmit = (vertrek, aankomst, tijd) =>{
      this.getMeldingen(vertrek);
      this.gettrips(vertrek, aankomst, tijd);
    }

// Ophalen reisinformatie van de NSAPI krijgt vertrek(stad/station) aankomst(stad/station) tijd in (ISOString) mee
    gettrips(vertrek, aankomst, tijd) {
      const API_KEY =`${process.env.REACT_APP_API_KEY}`;
      var url = 'https://gateway.apiportal.ns.nl/public-reisinformatie/api/v3/trips/?fromStation=' + vertrek + '&toStation=' + aankomst + '&dateTime=' + tijd + ':00&departure'
      // var url = 'https://ns-api.nl/reisinfo/api/v3/stations';
      var header = {
        headers: {'Ocp-Apim-Subscription-Key': "d523cffd1a2946728f945d4b03934dfb"}
      };
      axios.get(url, header)
        .then(response => {
          this.setState({
            trips: response.data.trips,
            isLoading: false
          });
        })
        .catch(error => this.setState({ error, isLoading: false }));
    }

// haalt alle meldingen van het opgegeven vertrekpunt op van de laravelAPI
    getMeldingen(vertrek) {
      var url = 'http://api.minimetro.nl/api/meldingen/' + vertrek
      axios.get(url)
        .then(response => {
          this.setState({
            meldingen: response.data.result,
            isLoading: false
          });
        })
    }


// Haalt alle stations namen op door middel van NS API -> wordt in een object gezet
// met key:"label" value:"stationnaam" zodat de Material ui autosuggest dit herkent.

    getStations() {
      const API_KEY_STATIONS =`${process.env.REACT_APP_API_KEY_STATIONS}`;
      const suggestions = []
      var url = 'https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/stations'
      var header = {
        headers: {'Ocp-Apim-Subscription-Key': API_KEY_STATIONS}
      };
      axios.get(url, header)
        .then(response => {
          for (var i = 0; i < response.data.payload.length; i++) {
          var naam = response.data.payload[i].namen.lang;
          suggestions.push({"label": naam})
          }
          this.setState({
            isLoading: false,
            sug: suggestions
          });
        })
        .catch(error => this.setState({ error, isLoading: false }));
    }


// ophalen van alle gegevens bij opstart
    componentDidMount() {
      this.gettrips();
      this.getStations();
    }
    componentDidUpdate(){

    }



// Het versturen van een melding naar de laravel API krijgt de volgende data mee:

    // "rit_datum_tijd": tijd in ISOString,
    // "checksum": id rit String,
    // "rit_URL" : url rit van NS String,
    // "melding" : melding nummer (1,2,3,4) String,
    // "bestemming" : bestemming rit String,
    // "vertrekPunt" : vertrekPunt rit String,

    postApiLaravel(checksum, datumTijd, vertrekPunt, bestemming, urlRit, melding){
      axios({
          method: 'post',
          url: 'http://api.minimetro.nl/api/rit',
          headers: {},
          data: {
            "rit_datum_tijd": datumTijd,
            "checksum": checksum,
            "rit_URL" : urlRit,
            "melding" : melding,
            "bestemming" : bestemming,
            "vertrekPunt" : vertrekPunt,
          }
          })  .then(function (response) {
          })
          // rit id opslaan in de local storage zodat er niet gespammed kan worden
          var sessie = localStorage.getItem('checksum')
          var sessieNew = new Array(sessie, checksum)
          localStorage.setItem('checksum', sessieNew);
          this.getMeldingen(vertrekPunt);
          this.componentDidMount();
        }


      handleIndustriesOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
      };

      handleIndustriesClose = () => {
        this.setState({ anchorEl: null });
      };



// Hier worden de ritten gerenderd en weergegeven
    render() {
      const { anchorEl } = this.state;
      const { isLoading, trips, meldingen } = this.state;
      return (
        <React.Fragment>
          <div>
              <Zoekbalk onSubmit={this.onSubmit}/>
              <div className="ritcontainer">
            {!isLoading ? (

              // loop voor elk rit
              trips.map(trip => {
                var progress = '';
                var drukte = '';
                var aantalMeldingen = new Array();
                var meldingNummers = new Array('');
                var meldingIcons = new Array();
                const { legs, actualDurationInMinutes, shareUrl, checksum, punctuality, crowdForecast } = trip;

                // drukte anders verwoorden en de juiste bar laten zien

                if(crowdForecast == "LOW"){
                  progress = <LinearProgress color="primary"/>
                  drukte = "Rustig"
                }
                else if (crowdForecast == "MEDIUM"){
                  progress = <LinearProgress color="primary"/>
                  drukte = "Matig"
                }
                else if (crowdForecast == "HIGH"){
                  progress = <LinearProgress color="secondary"/>
                  drukte = "Druk"
                }

                // loop voor elk melding
                meldingen.map(melding => {
                  var arrayMelding = melding.melding
                  // als de melding id overeen komt met rit id
                     if (melding.checksum == trip.checksum){
                       aantalMeldingen.push(arrayMelding);
                       // 1 maal de meldingnummer in een array plaatsen
                       if (meldingNummers.includes(arrayMelding) == false){
                          meldingNummers.push(arrayMelding)
                          // juiste icon tonen bij juiste melding
                          if (arrayMelding == 1) {
                            meldingIcons.push(<Timelapse className="red"/>)
                          }
                          else if (arrayMelding == 2) {
                            meldingIcons.push(<AvTimer className="red"/>)
                          }
                          else if (arrayMelding == 3) {
                            meldingIcons.push(<DepartureBoard className="green"/>)
                          }
                          else if (arrayMelding == 4) {
                            meldingIcons.push(<GroupIcon className="red"/>)
                          }
                   }
                  }

                });



                // data van de api voor in de HTML
                var datumTijd = legs[0].destination.plannedDateTime;
                var urlRit = shareUrl.uri;
                var vertrekPunt = legs[0].origin.name;
                var bestemming = legs[0].direction;
                var minutes = Math.floor(legs[0].stops[0].departureDelayInSeconds / 60);
                if (isNaN(minutes)) {minutes = ""}
                return (
                  <div className="rit_div" key={checksum} id={checksum}>
                  <div>
                     <ExpansionPanel>
                       <ExpansionPanelSummary
                         expandIcon={<AddIcon />}
                         aria-controls="panel1c-content"
                         id="panel1c-header">
                         <Grid container spacing={3}>
                         <Grid item xs={4}>
                         <Typography><p>Vertrekpunt:<br/><b>{legs[0].origin.name || "info niet beschikbaar"}</b></p></Typography>
                         </Grid>
                         <Grid item xs={4}>
                         <Typography ><p>Bestemming:<br/><b>{ legs.length > 1 ? legs[1].destination.name : legs[0].destination.name || "info niet beschikbaar"}</b></p></Typography>
                         </Grid>
                         <Grid item xs={4}>
                         <Typography><p>Meldingen: <b>{aantalMeldingen.length}</b><br/>{meldingIcons}</p></Typography>
                         <Typography><p>Spoor: <b>{legs[0].origin.plannedTrack}</b></p></Typography>
                         </Grid>
                         <Grid item xs={6}>
                           <span>Vertrek - Aankomst:</span><br/>
                           <b>{formatDate(new Date(legs[0].origin.plannedDateTime.replace("-", "/").replace("-", "/").replace("T", " "))) || "info niet beschikbaar"}</b>
                           <small><b className="darkBlue">{!!(minutes)?"+" + minutes:""}</b></small>
                           <ArrowForward id="forwardarrow"/>
                           <b>{formatDate(new Date(legs[legs.length -1].destination.plannedDateTime.replace("-", "/").replace("-", "/").replace("T", " "))) || "info niet beschikbaar"}</b>
                         </Grid>
                         <Grid item xs={4}>
                           <p>Duur:<br/><b> ± {legs.length > 1 ? actualDurationInMinutes : actualDurationInMinutes || "info niet beschikbaar"} min</b></p>
                         </Grid>
                         <Grid item xs={2}>
                          <Typography ><p>Drukte:<br/><b>{drukte}{progress}</b></p></Typography>
                         </Grid>
                         </Grid>
                       </ExpansionPanelSummary>
                       <ExpansionPanelDetails >
                         { legs.length > 1 ?
                           <div className="pointfloaters pointfloaters--left helewidth">
                             <Typography ><p>Tussen Transfer:<br/><b>{legs[0].destination.name || "info niet beschikbaar"}</b></p></Typography>
                           </div> : ""}
                           <div className="pointfloaters pointfloaters--right helewidth">
                             <Typography ><p>Eindbestemming:<br/><b>{legs[0].direction  || legs[0].direction || "info niet beschikbaar"}</b></p></Typography>
                           </div>
                           <div className="pointfloaters pointfloaters--left helewidth">
                            <div className="spacer--right">
                            <Typography ><p>Stiptheid:<br/><b>{punctuality}%</b></p></Typography>
                              <a href={shareUrl.uri}>Zie reis op NS-website</a>
                            </div>
                           </div>
                       </ExpansionPanelDetails>
                       <Divider />
                       <ExpansionPanelActions id="expansionBox">
                       {!JSON.stringify(localStorage.getItem('checksum')).includes(checksum) ? (
                         <React.Fragment>
                         <Button onClick={() => this.postApiLaravel(checksum, datumTijd, vertrekPunt, bestemming, urlRit, 1)} size="small" style={ frtButton } color="primary">Te Laat</Button>
                         <Button onClick={() => this.postApiLaravel(checksum, datumTijd, vertrekPunt, bestemming, urlRit, 2)} size="small" style={ scdButton } color="secondary">Te Vroeg</Button>
                         <Button onClick={() => this.postApiLaravel(checksum, datumTijd, vertrekPunt, bestemming, urlRit, 3)} size="small" color="thrid">Optijd</Button>
                         <Button onClick={() => this.postApiLaravel(checksum, datumTijd, vertrekPunt, bestemming, urlRit, 4)} size="small" style={ fohButton } color="fourth">Te Druk</Button>
                         </React.Fragment>
                        ):
                         "Bedankt voor uw melding"
                       }
                       </ExpansionPanelActions>
                       <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Klacht invullen</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8">
              <div className="card">
                <div className="card-header">MiniMetro Klachtenformulier</div>

                <div className="card-body">
                  <form onSubmit={this.handleSubmit.bind(this)}>
                    <Grid item xs={12}>
                      <TextField
                        id="standard-with-placeholder"
                        label="Naam"
                        placeholder="Type uw naam hier"
                        margin="normal"
                        type="text"
                        name="naam"
                        onChange={this.handlenaamChange.bind(this)}
                        value={this.state.naam}
                        />
                          </Grid>
                        <Grid item xs={12}>
                        <TextField
                          id="standard-with-placeholder"
                          label="E-mailadres"
                          placeholder="Type uw e-mailadres hier"
                          margin="normal"
                          type="text"
                          name="email"
                          onChange={this.handleemailChange.bind(this)}
                          value={this.state.email}
                          />
                          </Grid>
                          <Grid item xs={12}>
                          <TextField
                            id="standard-with-placeholder"
                            label="van/naar"
                            placeholder="Type uw vertrekpunt en bestemming hier"
                            margin="normal"
                            type="text"
                            name="van/naar"
                            onChange={this.handlevanNaarChange.bind(this)}
                            value={this.state.vanNaar}
                            />
                          </Grid>
                      <Grid item xs={12}>
                      <TextField
                            id="standard-textarea"
                            label="Klacht"
                            placeholder="Type uw klacht hier"
                            multiline
                            margin="normal"
                            name="klacht"
                            onChange={this.handleklachtChange.bind(this)}
                            value={this.state.klacht}
                      />
                      </Grid>
                      <Grid item xs={12}>
                      <Fab
                        id="submit_knop"
                        variant="extended"
                        size="small"
                        type="submit"
                        color="primary"
                        aria-label="Add">Verzenden
                      </Fab>
                      </Grid>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
                     </ExpansionPanel>
                   </div>
                  </div>
                );
              })
              ) : (
              <CircularUnderLoad/>
            )}
            </div>
          </div>
        </React.Fragment>
      );
    }
  }


  TreinInfo.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default TreinInfo;
