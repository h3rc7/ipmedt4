//importeren benodigde componenten
import React from "react";
import './App.css';
import RowingIcon from '@material-ui/icons/Rowing';
import axios from 'axios';

//Class voor weerkzaamheden
class Werkzaamheid extends React.Component{
  constructor(props){
    super(props);
    this.getStoring();
  }

  state = {
    trips: [],
    isLoading: true,
    errors: null
  };
//Ophalen van de werkzaamheden van de NS api
  getStoring = () =>{
    var url = 'https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/disruptions?type=Werkzaamheid&lang=nl';
    var header = {headers: {'Ocp-Apim-Subscription-Key': '29a47d4469b8424f8410d3b41d12927b'} };
    axios.get(url, header)
      .then(response => {
        this.setState({
          trips: response.data.payload,
          isLoading: false
        });
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }
// Renderen en weergeven van de weerkzaamheden die opgehaald worden door de ns api
  render(){
      const { isLoading, trips } = this.state;
    return(
      <div>
        <div className="tijd_div_topbar tijd_div_topbar--orange"  onClick={this.gettripsWerkzaamheden}>
            <RowingIcon color="disabled" style={{ fontSize: 30, marginTop: -9, marginRight: -25, marginLeft: 26  }}/>
            <h4 className="tijd_div_topbar__h4--orange">Werkzaamheid: ({trips.length}) </h4>
        </div>
        <div className="tijd_div tijd_div--modifier">
          { !isLoading && trips.length > 0 ? (
            trips.map(trip => {
              const { id, titel, type, verstoring } = trip;
              return(
                <div key={id} id="storing_enkelblok" >
                  <h2>{titel || "info niet beschikbaar"}</h2>
                  <p><b>Type:</b> {type ||"info niet beschikbaar"}</p>
                  <p><b>Oorzaak:</b> {verstoring.oorzaak || "info niet beschikbaar"}</p>
                  <p><b>Extra reistijd:</b> {verstoring.gevolg ||"info niet beschikbaar"}</p>
                  <hr className="hr__storing_enkelblok" />
                </div>
              );
            })
          ) : (
            <div id="storing" >
              <h3>Er zijn op dit moment geen storingen</h3>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default Werkzaamheid;
