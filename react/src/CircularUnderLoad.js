//importeren benodigde componenten
import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
// Functie voor loading icon
function CircularUnderLoad() {
  return <CircularProgress disableShrink />;
}

export default CircularUnderLoad;
