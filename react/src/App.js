//importeren benodigde componenten
import React from "react";
import TopBar from "./TopBar";
import BottomAppBar from "./BottomAppBar";
import CssBaseline from '@material-ui/core/CssBaseline';


  class App extends React.Component {

    render() {
      return (
        <React.Fragment>
        <CssBaseline />
        <TopBar/>
        <BottomAppBar/>
        </React.Fragment>
      );
    }
  }

export default App;
