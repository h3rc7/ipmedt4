<?php

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

use Illuminate\Http\Request;
Route::get('users','UserController@index');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/*
|-------------------------------------------------------------------------------
| Adds a New rit
|-------------------------------------------------------------------------------
| URL:            /api/v1/ritten
| Controller:     API\CafesController@postNewCafe
| Method:         POST
| Description:    Adds a new rit to the application
*/

// Route::post('/rit/new', function(Request $request) {
//     return Article::create($request->all);
// });

// Route::put('articles/{id}', function(Request $request, $id) {
//     $article = Article::findOrFail($id);
//     $article->update($request->all());
//
//     return $article;
// });


// Route::post('rit/new/{rit}', function ($ritMelding) {
//   // DB::insert('insert into ritten (rit) values (?)', [$ritMelding]);
//     // return $ritMelding;
//     var_dump("");
// });


Route::get('meldingen/{vertrek}', function($vertrek) {
  $att =  DB::table('meldingen')
     ->where('vertrek', $vertrek)
     ->get();
  return response()->json(['response' => 'success', 'result' => $att]);
});

Route::get('klachten', function() {
  $att =  DB::table('klachten')
     ->get();
  return response()->json(['response' => 'success', 'result' => $att]);
});

Route::post('rit', function(Request $request) {
  $ritDatumTijd = $request->input('rit_datum_tijd');
  $checksum = $request->input('checksum');
  $ritURL = $request->input('rit_URL');
  $melding = $request->input('melding');
  $bestemming = $request->input('bestemming');
  $vertrekpunt = $request->input('vertrekPunt');

  DB::table('meldingen')->insert([
      'checksum' => $checksum,
      'rit_datum_tijd' => $ritDatumTijd,
      'aankomst' => $bestemming,
      'vertrek' => $vertrekpunt,
      'melding' => $melding,
      'url' => $ritURL,
    ]);

    return  response()->json([
            'rit_datum_tijd' => $ritDatumTijd,
            'checksum' => $checksum,
            'rit_URL' => $ritURL,
            'melding' => $melding,
            'bestemming' => $bestemming,
            'vertrekpunt' => $vertrekpunt,
        ], 201);
});

Route::post('klacht', function(Request $request) {
  $naam = $request->input('naam');
  $vanNaar = $request->input('vanNaar');
  $email = $request->input('email');
  $klacht = $request->input('klacht');


  DB::table('klachten')->insert([
      'naam' => $naam,
      'vanNaar' => $vanNaar,
      'email' => $email,
      'klacht' => $klacht,
    ]);

    return  response()->json([
      'naam' => $naam,
      'vanNaar' => $vanNaar,
      'email' => $email,
      'klacht' => $klacht,
        ], 201);
});
