<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKlachtenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('klachten', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("naam");
            $table->string("vanNaar");
            $table->string("email");
            $table->string("klacht");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('klachten');
    }
}
