<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meldingen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("checksum");
            $table->string("rit_datum_tijd");
            $table->string("aankomst");
            $table->string("vertrek");
            $table->string("melding");
            $table->string("url");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rit');
    }
}
